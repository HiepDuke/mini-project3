
# Welcome to your CDK Python project to create AWS-S3 Bucket! by Hiep Nguyen

This is a project to create S3 Bucket with CDK in Python.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python3 -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

Add S3 Bucket stack to create_s3_cdk/create_s3_cdk_stack.py

```
import aws_cdk as cdk
import aws_cdk.aws_s3 as s3

class CreateS3CdkStack(Stack):

    def __init__(self, scope: cdk.App, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        # The code that creates a S3 bucket
        bucket = s3.Bucket(self, "MyFirstBucket", versioned=True)
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

After that you can now deploy it to AWS if you already use Cloud9 otherwise login with your credential to AWS.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

Enjoy!

## Some screenshot before and after deployment

This is expected when you set up the project!

![Image Alt Text](./images/Screenshot_2024-02-16_102339.png)


![Alt Text](./images/Screenshot_2024-02-16_102533.png)

![Alt Text](./images/Screenshot_2024-02-16_102300.png)

![Alt Text](./Screenshot_2024-02-16_65948.png)