import aws_cdk as core
import aws_cdk.assertions as assertions

from create_s3_cdk.create_s3_cdk_stack import CreateS3CdkStack

# example tests. To run these tests, uncomment this file along with the example
# resource in create_s3_cdk/create_s3_cdk_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = CreateS3CdkStack(app, "create-s3-cdk")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
