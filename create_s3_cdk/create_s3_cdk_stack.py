from aws_cdk import (
    # Duration,
    Stack,
    # aws_sqs as sqs,
)
import aws_cdk as cdk
import aws_cdk.aws_s3 as s3

class CreateS3CdkStack(Stack):

    def __init__(self, scope: cdk.App, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        # The code that creates a S3 bucket
        bucket = s3.Bucket(self, "MyFirstBucket", 
        versioned=True,
        removal_policy=cdk.RemovalPolicy.DESTROY,
        auto_delete_objects=True)
        # example resource
        # queue = sqs.Queue(
        #     self, "CreateS3CdkQueue",
        #     visibility_timeout=Duration.seconds(300),
        # )
